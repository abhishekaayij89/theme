$(document).ready(function () {

  //===========top menu list click ==================
  $(document).on("click", ".top-menu .lng", function () {

    $(".currency").removeClass("active");
    $(this).toggleClass("active");
  })


  $(document).on("click", ".top-menu .currency", function () {

    $(".lng").removeClass("active");
    $(this).toggleClass("active");
  })

  //= Mobile Responsive nav bar click

  $(document.body).on("click", ".nav-bar", function () {
    $(document.body).addClass("overlay");
    $(".menu-list").addClass("response-nav");

  });

  //===========show active / inactive menu 

  $(document.body).on("click", ".has-submenu", function () {
    $(this).toggleClass("open");
  })

  $(document.body).on("click", ".close-nav-menu", function () {
    $(document.body).removeClass("overlay");
    $(".menu-list").removeClass("response-nav");
  })


  //====== color active

$(document.body).on("click",".color li",function(){
  $(this).addClass('active').siblings().removeClass('active');
});

  //================= Offer Slider

  $('.offer-slider').slick({
    autoplay: true,
    autoplaySpeed: 2000,
    speed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    dots: false,
    arrows: true,
    nextArrow: '.offer-next',
    prevArrow: '.offer-prev',
  });


  //================= Store Slider

  //most vvi example 

  $('.store-slider').slick({
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 7,
    arrows: false,
    dots: false,
    autoplay: true,
    speed: 1000,
    // prevArrow: '<button class="slide-arrow prev-arrow"><i class="fal fa-long-arrow-left"></i></button>',
    // nextArrow: '<button class="slide-arrow next-arrow"><i class="fal fa-long-arrow-right"></i></button>',
    responsive: [
      {
        breakpoint: 1399,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 6
        }
      },
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 479,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });



  //=========== home slider 

  $('.h-slider').slick({

    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    autoplay: false,
    arrows:false,
    customPaging: function(slick,index) {
      return '<a></a>';
  }

  });


  //========= Deal Slider


  $('.deal-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: false,
    autoplay: false,
    arrows: true,
     nextArrow: '.next-deal',
     prevArrow: '.prev-deal',
    responsive: [
  
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },

      {
        breakpoint: 579,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });


  //======== Testimonal Test

  $('.testimonial-list').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    autoplay: false,
    arrows: true,
     nextArrow: '.next-deal',
     prevArrow: '.prev-deal',
     
    responsive: [
  
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },

      {
        breakpoint: 579,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  //===================== Don't touch ============================

})