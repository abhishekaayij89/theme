$(document).ready(function(){

//================================ Document Ready Function 

    //  hand slick slider 


jQuery(window).on('scroll', function () {
    if (jQuery(window).scrollTop() > 30) {
        jQuery('.navigation').addClass("sticky");
        jQuery('.b-box').addClass("move-top");


    } else {
        jQuery('.navigation').removeClass("sticky");
    }
    
});



$('.logo-list').slick({
  mobileFirst:true,
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    dots: false,
    prevArrow: false,
    nextArrow: false,
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 3,
           
          }
        },
        {
          breakpoint: 785,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        

        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
});



//testimonial slick slider

$('.testimonial-slider').slick({
    // infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 9000,
    dots: true,
    prevArrow: false,
    nextArrow: false,
    responsive: [
      {
        breakpoint: 1028,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
         
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
         
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
});





// Light box slider

var $box = $(".isotope-box").isotope({
    itemSelector: ".isotope-item"
});
// filter functions
// bind filter button click
$(".isotope-toolbar").on("click", "a", function () {
    var filterValue = $(this).attr("data-type");
    $(".isotope-toolbar-btn").removeClass("active");
    $(this).addClass("active");
    if (filterValue !== "*") {
        filterValue = '[data-type="' + filterValue + '"]';
    }
    console.log(filterValue);
    $box.isotope({ filter: filterValue });
});



//================= Responsive web site 
$(document.body).on("click",".open-menu",function(){
    $('body').addClass("mobile-nav");
    $(this).addClass("d-none");
    $(".close-menu").removeClass("d-none");
    $('.menu-list').addClass("responsive-menu");
});

$(document.body).on("click",".close-menu",function(){
    $('body').removeClass("mobile-nav");
    $(this).addClass("d-none");
    $(".open-menu").removeClass("d-none");
    $('.menu-list').removeClass("responsive-menu");
})




//================================ End Document Ready 
})


