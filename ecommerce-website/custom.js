$(Document).ready(function () {

  //=== header select lang
  $(document).on("click", ".select-lang", function () {
    $(".lang").toggleClass("open");
  });

  //=== header select currency

  $(document).on("click", ".select-country", function () {
    $(".currency").toggleClass("open");
  })


  //================= SLICK SLIDER ==========================

  $('.header-banner-list').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: false,
    // autoplaySpeed: 3000,

  });



  //=================== TOP PRODUCT LIST SLICK SLIDER =============
  $('.top-products').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    nextArrow: '.slick-next',
    prevArrow: '.slick-prev',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,

        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 850,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,

        }
      },

      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]

  });



  //================ image move effect =====================

  /* $(".deal-img-area").on("mouseenter", function (e) {
    let x = e.pageX - $(this).offset().left;
    let y = e.pageX - $(this).offset().top;
    $(this).find(".img-sweap").css({ top: y, left: x ,display:"block"});

  })


  $(".deal-img-area").on("mouseout", function (e) {
    let x = e.pageX - $(this).offset().left;
    let y = e.pageX - $(this).offset().top;
    $(this).find(".img-sweap").css({ top: y, left: x ,display:"none"});
  })

 */


  //=================== DEAL OF THE DAY SLICK SLIDER =============

  $('.deal-product-list').slick({
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: true,
    nextArrow: '.deal-next',
    prevArrow: '.deal-prev',
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1

        }
      }
    ]

  });


  //===================== Custom select tab 

  $(document).on("click", ".newfeature-product .product-heading", function () {
    $(".newfeature-product .product-heading").addClass("disabled")
    $(this).removeClass("disabled");
  });

  $(".tp").css({ display: "none" })

  $(document).on("click", ".new-pro", function () {
    $(".np").addClass("active").css({ display: "block" });
    $(".tp").removeClass("active").css({ display: "none" });
    $(".new-pro-move").removeClass("d-none");
    $(".top-fea-move").addClass("d-none");
    $('.new-product-list').slick('reinit').slick();

  });

  $(document).on("click", ".top-fea", function () {
    $(".tp").addClass("active").css({ display: "block" });
    $(".np").removeClass("active").css({ display: "none" });
    $(".new-pro-move").addClass("d-none");
    $(".top-fea-move").removeClass("d-none");
    $('.top-featured-list').slick('reinit').slick();
  });



  //=================== TOP PRODUCT LIST SLICK SLIDER =============

  $('.new-product-list').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    nextArrow: '.move-top-next',
    prevArrow: '.move-top-prev',


    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 850,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      },

      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]

  });

  //===================== Top Featured 

  $('.top-featured-list').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    arrows: false,
    nextArrow: '.move-fea-next',
    prevArrow: '.move-fea-prev',
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 850,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      },

      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]

  });


  //================== Best Seller List

  $('.best-seller-list').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    nextArrow: '.best-seller-next',
    prevArrow: '.best-seller-prev',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,

        }
      }, {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,

        }
      }
    ]

  });


  //================== Discount Product List   //best slick example

  $('.discount-product-list').slick({
    mobileFirst: true,
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: false,

    responsive: [
      {
        breakpoint: 1400,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,

        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,

        }
      },

    ]

  });

  // news list slider 

  $('.news-list').slick({
    // mobileFirst: true,
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    arrows: false,
    responsive: [

      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,

        }
      },

    ]


  });


  // compnay profile slider 

  $('.compnay-list').slick({
    mobileFirst: true,
    dots: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 0,
    speed: 3000,
    cssEase: 'linear',
    focusOnSelect: true,
    pauseOnHover: true,
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,

        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,

        }
      },

      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,

        }
      },

    ]
  });


  //=========== modal close ===============


  $(document.body).on("click", ".modal .close", () => {
    sessionStorage.setItem("show-modal", true);
    $(document.body).removeClass('overlay');
    $(".modal").addClass("d-none");
  });

  if (sessionStorage.getItem("show-modal")) {
    $(".modal").addClass("d-none");
    $(document.body).removeClass('overlay');
  } else {
    $(".modal").removeClass("d-none");
    $(document.body).addClass('overlay');

  }

  //============= add active class



$(".nav-menu-child").hover(function () {
  $(this).prev(".side_name").toggleClass("active");
});




  //============= product heading 

  $(document.body).on("click", ".product-heading", function () {
    $(".product-heading").not(this).parent(".active").removeClass("active");
    $(".product-heading").not(this).next(".do-slide").slideUp("slow");
    $(this).parent().toggleClass("active");
    $(this).next(".do-slide").slideToggle("slow");

  });


  //========= scroll to top

      $(window).on("scroll", function () {
        var scrollPos = $(window).scrollTop();
        if (scrollPos <= 100) {
          $(".move-up").fadeOut();
        } else {
          $(".move-up").fadeIn();
        }
      });



  $(document.body).on("click", ".move-up", function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
  });


  //==========menu header click

  $(document.body).on("click",".cat-menu",function(){
    $(".sec-menu").toggleClass("active");
  });


  $(document.body).on("click",".menu-icon",function(){
    $(".sec-menu").toggleClass("active");
    $(document.body).addClass('menu-overlay');
  });

  // $(document.body).on("click",".nav-menu",function(){
  //      $(this).toggleClass("active");
  // });

  


  //====================================== END =======================   
})


